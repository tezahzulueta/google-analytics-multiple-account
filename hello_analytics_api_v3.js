var _ = require('underscore')._;

function makeApiCall() {
  queryAccounts();
}

function queryAccounts() {
  console.log('Querying Accounts.');

  // Get a list of all Google Analytics accounts for this user
  gapi.client.analytics.management.accounts.list().execute(handleAccounts);
}

function handleAccounts(results) {
  //log the result
  var json = JSON.stringify(results);
  console.log("JSON", json);

  if (!results.code) {
    if (results && results.items && results.items.length) {
      //print total number of users account
      console.log("COUNT: ", results.items.length);
      //add Google Analytics Account in Dropdown List name "accounts" 
      var acct = document.getElementById("accounts");
      for (var i=0; i<results.items.length; i++){
        console.log("RESULTS: ", results.items[i].id);  
        var acct_option = document.createElement("option");
        acct_option.text = results.items[i].name;
        acct_option.value = results.items[i].id;
        acct.add(acct_option); 
      }
      //validation 
      if(results.items.length > 1){
        $( "#accounts" ).change(function(accounts) {
          $( "#webPropertyID" ).empty();
          var accountId = $("#accounts").val();
          console.log("ACCOUNT ID:", accountId);
          console.log('Querying Webproperties.');
          // Get a list of all the Web Properties for the account
          gapi.client.analytics.management.webproperties.list({'accountId': accountId}).execute(handleWebproperties);
        });  
      }
      else{
        $( "#webPropertyID" ).empty();
        var accountId = $("#accounts").val();
        console.log("ACCOUNT ID:", accountId);
        console.log('Querying Webproperties.');
        // Get a list of all the Web Properties for the account
        gapi.client.analytics.management.webproperties.list({'accountId': accountId}).execute(handleWebproperties);  
      }

    } else {
      console.log('No accounts found for this user.')
    }
  } else {
    console.log('There was an error querying accounts: ' + results.message);
  }
}

function handleWebproperties(results) {
  console.log("JSON", results);
  console.log("ACCOUNT ID: ", results.items[0].accountId);
  var account_id = results.items[0].accountId;

  if (!results.code) {
    if (results && results.items && results.items.length) {
      //print total number of users account
      console.log("COUNT: ", results.items.length);

      var webPropertyID = document.getElementById("webPropertyID");
      //print all account id under the ff. user
      for (var i=0; i<results.items.length; i++){
        console.log("Account ID: ", results.items[i].accountId);
        console.log("Web Property ID: ", results.items[i].id);  
        console.log("Web Property Name: ", results.items[i].name);  
        var webProperty_option = document.createElement("option");
        webProperty_option.text = results.items[i].name;
        webProperty_option.value = results.items[i].id;
        webPropertyID.add(webProperty_option); 
      }
      if(results.items.length > 1){
        $( "#webPropertyID" ).change(function(webPropertyID) {
          $( "#viewID" ).empty();
          console.log("WEB: ", webPropertyID);
          var webpropertyIds = document.getElementById("webPropertyID").value;
          console.log("SELECTED WEB PROPERTY ID: ", webpropertyIds);
          
          // Query for Views (Profiles)
          queryProfiles(account_id, webpropertyIds);
        
        }); 
      }
      else{
        $( "#viewID" ).empty();
        var webpropertyIds = $("#webPropertyID").val();
        console.log("WEB PROPERTY ID: ", webpropertyIds);
        console.log("Querying Profile Properties");
        // Query for Views (Profiles)
        queryProfiles(account_id, webpropertyIds);  
      } 

    } else {
      console.log('No webproperties found for this user.');
    }
  } else {
    console.log('There was an error querying webproperties: ' + results.message);
  }
}

function queryProfiles(accountId, webpropertyId) {
  console.log('Querying Views (Profiles).');

  // Get a list of all Views (Profiles) for the first Web Property of the first Account
  gapi.client.analytics.management.profiles.list({
      'accountId': accountId,
      'webPropertyId': webpropertyId
  }).execute(handleProfiles);
}

function handleProfiles(results) {
  console.log(results);

  if (!results.code) {
    if (results && results.items && results.items.length) {
      //print total number of users account
      console.log("COUNT: ", results.items.length);

      var view_id = document.getElementById("viewID");
      //print all account id under the ff. user

      for (var i=0; i<results.items.length; i++){
        console.log("RESULTS: ", results.items[i].id);  
        var view_option = document.createElement("option");
        view_option.text = results.items[i].name;
        view_option.value = results.items[i].id;
        view_id.add(view_option); 
      }
      if(results.items.length > 1){
        $( "#viewID" ).change(function(view_ids) {
          var profile_id = document.getElementById("viewID").value;
          console.log("SELECTED PROFILE ID: ", profile_id);         
          // Step 3. Query the Core Reporting API
          queryCoreReportingApi(profile_id);
        
        });  
      }
      else{
        var profile_id = $("#viewID").val();
        console.log("VIEW ID: ", profile_id);
        // Step 3. Query the Core Reporting API
        queryCoreReportingApi(profile_id); 
      }

    } else {
      console.log('No views (profiles) found for this user.');
    }
  } else {
    console.log('There was an error querying views (profiles): ' + results.message);
  }
}

function queryCoreReportingApi(profile_id) {
  console.log(profile_id);
  
  console.log('Querying Core Reporting API.');

  // Use the Analytics Service Object to query the Core Reporting API
  gapi.client.analytics.data.ga.get({
    'ids': 'ga:' + profile_id,
    'start-date': '2015-01-01',
    'end-date': '2015-02-01',
    'metrics': 'ga:sessions'
  }).execute(handleCoreReportingResults);
}

function handleCoreReportingResults(results) {
  if (results.error) {
    console.log('There was an error querying core reporting API: ' + results.message);
  } else {
    printResults(results);
  }
}

function printResults(results) {
  var json = JSON.parse(this.responseText);
  console.log("JSON", json);

  if (results.rows && results.rows.length) {
    console.log('Table ID: ',results.profileInfo.tableId);
    console.log('View (Profile) Name: ', results.profileInfo.profileName);
    console.log('Total Sessions: ', results.rows[0][0]);
  } else {
    console.log('No results found');
  }
}